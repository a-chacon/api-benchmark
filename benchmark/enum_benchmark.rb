require 'benchmark'
require_relative '../config/environment'

n = 1000

Benchmark.bm(35) do |x|
  # Count records
  x.report('String Enum Count:') do
    n.times do
      ActiveRecord::Base.connection.execute("SELECT COUNT(*) FROM enum_strings WHERE status = 'draft'")
    end
  end

  x.report('Integer Enum Count:') do
    n.times do
      ActiveRecord::Base.connection.execute('SELECT COUNT(*) FROM enum_integers WHERE status = 0')
    end
  end

  # Paginated index (limit and offset)
  x.report('String Enum Paginated Index:') do
    n.times do
      ActiveRecord::Base.connection.execute("SELECT * FROM enum_strings WHERE status = 'active' LIMIT 20 OFFSET 20")
    end
  end

  x.report('Integer Enum Paginated Index:') do
    n.times do
      ActiveRecord::Base.connection.execute('SELECT * FROM enum_integers WHERE status = 1 LIMIT 20 OFFSET 20')
    end
  end

  # Fetch a single record
  x.report('String Enum Single Record Fetch:') do
    n.times do
      ActiveRecord::Base.connection.execute('SELECT * FROM enum_strings WHERE status = "archived" LIMIT 1')
    end
  end

  x.report('Integer Enum Single Record Fetch:') do
    n.times do
      ActiveRecord::Base.connection.execute('SELECT * FROM enum_integers WHERE status = 2 LIMIT 1')
    end
  end
end
