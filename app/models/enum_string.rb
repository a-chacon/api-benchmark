class EnumString < ApplicationRecord
  enum status: { draft: 'draft', active: 'active', archived: 'archived' }
end
