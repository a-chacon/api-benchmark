class EnumInteger < ApplicationRecord
  enum :status, %i[draft active archived]
end
