class CreateEnumIntegers < ActiveRecord::Migration[7.1]
  def change
    create_table :enum_integers do |t|
      t.string :title
      t.text :description
      t.integer :status

      t.timestamps
    end
  end
end
