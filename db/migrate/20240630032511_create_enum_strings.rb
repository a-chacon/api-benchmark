class CreateEnumStrings < ActiveRecord::Migration[7.1]
  def change
    create_table :enum_strings do |t|
      t.string :title
      t.text :description
      t.string :status

      t.timestamps
    end
  end
end
